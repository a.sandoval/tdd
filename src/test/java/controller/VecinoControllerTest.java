package controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test de crear vecino")
class VecinoControllerTest {
    VecinoController vecinoController;

    @BeforeEach
    void setUp() {
        vecinoController = new VecinoController();
    }

    @Test
    @DisplayName("Verificar texto no nulo")
    public void testTextoNulo(){
        assertTrue(vecinoController.validarTexto("Andy"));
    }

    @Test
    @DisplayName("Verificar texto vacio")
    public void testTextoVacio(){
        assertTrue(vecinoController.validarTexto("Andy"));
    }

    @Test
    @DisplayName("Verificar contenido del texto")
    public void testValidarNombreApellidoDir(){
        assertTrue(vecinoController.validarContenidoTexto("Andy"));
        assertTrue(vecinoController.validarContenidoTexto("Sandoval"));
    }

    @Test
    @DisplayName("Verificar numero telefonico")
    public void testValidarNumeroTelefonico(){
        assertTrue(vecinoController.validarNumeroTelefonico(977323817));
    }

    @Test
    @DisplayName("Verificar numero direccion")
    public void testValidarNumeroDireccion(){
        assertTrue(vecinoController.validarNumeroDireccion(1803));
    }

    @Test
    @DisplayName("Verificar run")
    public void testValidarRun(){
        assertTrue(vecinoController.validarRun("66.666.666-6"));
    }

    @Test
    @DisplayName("Verificar contrasena")
    public void testValidarContrasena(){
        assertTrue(vecinoController.validarContrasena("*Ingsoft2022"));
    }

    @AfterEach
    void tearDown() {
    }
}