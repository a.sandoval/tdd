package controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VecinoController {

    public boolean validarTexto(String texto){
        if(texto == null || texto == ""){
            return false;
        }else{
            return true;
        }
    }

    public boolean validarContenidoTexto(String nombre){
        boolean esNombre = true;
        for (int i = 0; i < nombre.length(); i++) {
            if (Character.isDigit(nombre.charAt(i))) {
                esNombre = false;
                break;
            }
        }
        return esNombre;
    }

    public boolean validarNumeroTelefonico(int numero){
        String nroTel = Integer.toString(numero);
        if(nroTel.length() == 9){
            return true;
        }else{
            return false;
        }
    }

    public boolean validarNumeroDireccion(int numero){
        if(numero>0){
            return true;
        }else{
            return false;
        }
    }

    public boolean validarRun(String run) {
        boolean validacion = false;
        try {
            run =  run.toUpperCase();
            run = run.replace(".", "");
            run = run.replace("-", "");
            int rutAux = Integer.parseInt(run.substring(0, run.length() - 1));

            char dv = run.charAt(run.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    public boolean validarContrasena(String pass){
        String regex = "^(?=.*[0-9])"
                   + "(?=.*[a-z])(?=.*[A-Z])"
                   + "(?=.*[@#$%^&+=*])"
                   + "(?=\\S+$).{8,15}$";

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(pass);
        return m.matches();
    }
}